#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(){
	int i;
	int cont;
	int status;
	
	for(i=0; i<10; i++){
		if (fork()==0){        // caso seja processo filho
			for(cont=(i*100)+1; cont <= (i+1)*100; cont++){
				printf("N�mero %d ", cont);
			}
			printf("\n");
			printf("Novo processo");
			printf("\n");
			exit(2);
		}
	}
	
	for(i=0; i<10; i++){
		wait(NULL);
	}
	
	return 0;
}
