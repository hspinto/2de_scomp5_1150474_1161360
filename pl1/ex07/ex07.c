#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define ARRAY_SIZE 1000
int main() 
{

int numbers[ARRAY_SIZE];     /* array to lookup */ 
int n;						/*the number to find */ 
time_t t;					/*needed to init. the 
random number generator (RNG) */ 

int i;

pid_t pid;
	
	int res = 0, status;
	
	/* initializes RNG (srand():stdlib.h; time(): time.h) */
	srand((unsigned) time (&t));
	
	/* initialize array with random numbers (rand(): stdlib.h) */
	for (i = 0; i < ARRAY_SIZE; i++) {
		numbers[i] = rand() % 10000;
	}
	
	/* initialize n */
	n = rand() % 10000;
	
	pid = fork();
	
	if (pid < 0){ //error
		perror("\nFork falhou.\n");
		exit(-1);
	}
	if (pid == 0) { //filho vai ler a primeira metade
		// printf("\n\nn: %d\n",n);
		for (i = ARRAY_SIZE / 2; i < ARRAY_SIZE; i++) {
			if (numbers[i] == n) {
				res++;
			}
		}
		exit(res);
	}else{	
	
		for(i= 0 ; i < ARRAY_SIZE/2; i++){
			if(numbers[i] == n){
				res++;
			}
		}
	}
	// espera pelo processo terminar preenche o status
	waitpid(pid,&status,0);
	
	if (WIFEXITED(status)) {
		res += WEXITSTATUS(status);
	} else {
		printf("\nProcesso filho terminou com erro");
		exit(-1);
	}
	
	printf("\nNúmero de vezes que '%d' foi encontrado: %d\n",n,res);
	
	return 0;
}
