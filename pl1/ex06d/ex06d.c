#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h> 

int main () 
{
    int i;
    int status;
    int pid[4];
    int aux;

    for (i = 0; i < 4; i++)
    {
		pid[i] = fork();
		aux = getpid();
        if (pid == 0) {
            sleep(1);		/*sleep(): unistd.h */
             if(aux % 2 == 0){
				waitpid(aux,&status,0);
				while(status!=0 || status != 1){
					waitpid(aux,&status,0);
					printf("%d, %d \n", pid[i],WEXITSTATUS(status));
					sleep(1);
				}
			 }
        }	
    }
    
    printf ("This is the end.\n");
    printf("i= %d \n", i);
}
