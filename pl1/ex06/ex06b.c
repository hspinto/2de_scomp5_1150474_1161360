#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main(){

int i;
int status;
pid_t pid;

for(i=0; i<4; i++){
	pid=fork();
	
	if(pid==0){
		exit(0);  /*sleep(); unistd.h*/
	}else{
	printf("proceso %d--", pid);
	}
}

/*
for(i=0; i<4; i++){
	if(pid ==0)
		exit(0);  kill child process*
	}
*/
	 WEXITSTATUS(status);
	 
	printf("/this is the end.\n");
	return 0;
}
