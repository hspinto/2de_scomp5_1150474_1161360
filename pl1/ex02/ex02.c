#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main(){
	pid_t pid;
	int i;
	
	printf("I’m..\n");
	
	for(i=0; i<3; i++){
		pid = fork();
		if(pid>0 && i==0){
			printf("the.. \n");
		}
		
		if(pid>0 && i==1){
			printf("father!\n");
		}
		
		if(pid == 0){
			printf("i'll never join you!\n");
			exit(1);
		}
	}
	
	return 0;
	
}
			
			
			
			
