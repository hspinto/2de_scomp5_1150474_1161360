#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define CHILDS 10
#define SIZE 20

static const int READ = 0;

static const int WRITE = 1;

struct StRound{
	int round;
	char message[SIZE];
};

int main(void)
{
	struct StRound s = {0,"Win"}; //inicio do jogo
	pid_t pid[CHILDS];
	int fd[2]; 
	int i, status;
	
	if(pipe(fd) == -1)
	{
		perror("Pipe failed");
		exit(-1);
	}
	
	for(i = 0; i < CHILDS; i++)
	{
		pid[i] = fork();
		
		if(pid[i] == -1)
		{
			perror("Fork failed");
			exit(-1);
		}
		
		if(pid[i] == 0) 
		{
			close(fd[WRITE]);
			read(fd[READ], &s, sizeof(s));
			printf("I %s the round:%d and my PID is: %d\n",s.message,s.round, getpid());
			close(fd[READ]);
			exit(s.round);
		}
	}
	
	close(fd[READ]);
	for(i = 0; i < CHILDS; i++)
	{
		s.round = i + 1;
		write(fd[WRITE], &s, sizeof(s));
		sleep(1);
	}
	close(fd[WRITE]);
	for(i=0; i < CHILDS; i++)
	{
		waitpid(pid[i], &status, 0);
		if(WIFEXITED(status))
		{
			printf("Dad => My Son with the PID: %d Won the round: %d \n",pid[i],WEXITSTATUS(status));
		}
	}

	return 0;
}
