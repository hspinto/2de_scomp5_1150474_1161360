#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>

static const int BUFFER_SIZE = 255;


static const int READ = 0;


static const int WRITE = 1;

int main(void){
	pid_t pid;
	int fd[2];
	char fileRead[BUFFER_SIZE];
	
		if(pipe(fd) == -1){
				perror("Pipe failed!");
		exit(-1);
		}
		
		pid = fork();
		if(pid==-1){
			perror("Fork Failed");
			exit(-1);
		}
		
		if(pid>0){
			close(fd[READ]);
				char fileName[100] = "textfile.txt";
		
		FILE *file = fopen(fileName, "r+"); //r+ permite escrever e ler do ficheiro e se nao existir cria
		
		printf("O pai escreve: %s\n",fileName);
		
		char currentChar;
		int countFile = 0;
		while((currentChar = fgetc(file)) != EOF){
			fileRead[countFile] = currentChar;	//descobrir o tamanho do ficheiro
            countFile++;
        }
        
        fclose(file);
               
        if(fileRead[countFile-1] == '\n' || fileRead[countFile-1] == '\r'){
			fileRead[countFile] = '\0';	
		}//alterar os dados de modo a terminar com o padrão \0
		
		write(fd[WRITE], fileRead, BUFFER_SIZE);	//escreve
		close(fd[WRITE]);
		
		}else{
		close(fd[WRITE]);
		
        read(fd[READ], fileRead, BUFFER_SIZE);  	//lê
		printf("Filho lê:\n %s", fileRead);
		
		close(fd[READ]);
	}
	wait(NULL);
	return 0;
}
