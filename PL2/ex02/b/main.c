#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define BUFFER_SIZE 255


static const int READ = 0;


static const int WRITE = 1;

struct StructForPipe {
	int integerValue;
    char string[BUFFER_SIZE];
};

int main(void)
{
	pid_t pid;
	struct StructForPipe s;
	int fd[2];
	
	if(pipe(fd) == -1){
		perror("Pipe failed!");
		exit(-1);
	}
	
	pid = fork();
	if(pid == -1){
		perror("Fork failed!");
		exit(-1);
	}
	
	if(pid > 0){
		close(fd[READ]); //fecha read pq vamos escrever

		printf("inserir um valor inteiro ");
		scanf("%d%*c",&s.integerValue);
		
		printf("inserir uma String ");
		fgets(s.string,BUFFER_SIZE,stdin);
		write(fd[WRITE], &s, sizeof(s));
		close(fd[WRITE]);
		wait(NULL);
	}else{
		close(fd[WRITE]);

		while(read(fd[READ], &s, sizeof(s)) == sizeof(s)){
		printf("Filho lê, O valor do inteiro é: %d\n", s.integerValue);
		printf("O valor da string é: %s\n",s.string);
		}
		close(fd[READ]);
		exit(1);
	}

	return 0;
}
