#include <stdio.h> //operações de I/O
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

static const int BUFFER_SIZE = 50;

static const int READ = 0;

static int WRITE = 1;

int main(void){
	pid_t pid;
	int integerVariable;
	char string[BUFFER_SIZE];
	int fd[2];
	int stringLength;
	
	if(pipe(fd) == -1){
		perror("Pipe failed");
		exit(-1); //valor a retornar no erro do processo
	}
	
	pid = fork();
	if(pid == -1){	//no caso de falhar o fork
		perror("Fork failed!");
		exit(-1);
	}
	
	if(pid > 0){
		close(fd[READ]);  //vamos escrever
		printf("Processo Pai \n");
		printf("inserir um valor inteiro ");
		scanf("%d%*c",&integerVariable);
		
		printf("inserir uma String ");
		fgets(string,BUFFER_SIZE,stdin);
		//abre a escrita
		write(fd[WRITE], &integerVariable, sizeof(int));	

		stringLength = strlen(string)+1;
		
		write(fd[WRITE], &stringLength, sizeof(int)); 
		write(fd[WRITE], &string, sizeof(char)*stringLength);
		close(fd[WRITE]);	//termina a escrita
		wait(NULL);
	}else{
		close(fd[WRITE]); //fecha a escrita para iniciar a leitura pelo processo filho
		if(read(fd[READ], &integerVariable, sizeof(int)) == sizeof(int)){
			printf("Filho lê, O valor do inteiro é: %d\n",integerVariable);
		}
		if(read(fd[READ], &stringLength, sizeof(int)) == sizeof(int)){
			while(read(fd[READ], &string, sizeof(char)*stringLength)== sizeof(char)*stringLength);
			printf("O valor da string é: %s\n", string);
		
		close(fd[READ]);
		}
		exit(1);
	}
	return 0;
}
		
		
		
		
